import React from 'react';
import Head from 'next/head';

const Layout = (props) => {
    return (
        <>
            <Head>
                <title>GitHub Jobs</title>
                <meta charset="UTF-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w==" crossorigin="anonymous" />
                <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet" />
            </Head>
            <div className="w-full pt-4 pb-4 pr-32 pl-32 bg-gray-100 h-screen max-h-screen">
                {props.children}
            </div>
        </>
    );
}

export default Layout;